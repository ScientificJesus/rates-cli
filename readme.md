# Rates-cli

Rates-cli is a Python client for parsing bank.gov.ua rates.

---
## Requirements

  * Running Redis-server >=5.0.7
  * PostgreSQL >=10
  * Python >=3.7

---
## Installation

Pull the project. Navigate to the directory and run:

```bash
pip install rates-cli
```
---
## Usage

```bash
rates-cli --date=27-07-2021
```

to parse one date or:
```bash
rates-cli --date=27-07-2021 -r
```
to parse all the dates between specified one and today.