from setuptools import setup, find_packages


setup(
    name='rates-cli',
    version='0.0.1',
    description='This package saves rates from bank.gov.ua to psql db',
    author='Dmytro Padalka',
    author_email='test@test.com',
    install_requires=[
        'aiohttp==3.7.4',
        'aiopg==1.3.1',
        'arq==0.21',
        'trafaret==2.1.0',
        'arrow==1.1.1',
        'sqlalchemy==1.3.21',
        'sqlalchemy_utils==0.37.8',
        'click==8.0.1'
    ],
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'rates-cli=rates.client:main'
        ]
    },
)