import logging
import os
import sys
import asyncio
import aiohttp
import arq
import arrow
import click
import trafaret

from .utils.db import Postgresql
from .utils.exceptions import ResponseCodeError
from .utils.trafarets import currency_trafaret

LOG = logging.getLogger(__name__)

help_str = 'Client for bank.gov.ua rates.\n'\
            'Usage: rates-cli 20-07-2021\n'\
            'Provide date as an argument to start getting rates from\n'\
            'Clienti will process all of the dates and keep getting new for today every 5 minutes\n'\
            '-h, --help  ——  print this text'\


class BankClient:
    def __init__(self, date):
        self.date = date
        self.bank_url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange'
        self.data_folder = 'data'
        self.psql = Postgresql()

    async def _get(self, url, params):
        '''
        Async get method

        :param url: url to send request to
        :type url: str

        :param params: parameters to add to request
        :type params: dict/str/list

        :return: response from url
        :rtype: list

        '''

        async with aiohttp.ClientSession() as session:
            async with session.get(url, params=params) as resp:
                if resp.status != 200:
                    msg = f'Response code is not 200. Got {resp.status} instead.'
                    LOG.error(msg)
                    raise ResponseCodeError(msg)

                LOG.info(f'Successful get request')
                json = await resp.json()

                if not json:
                    raise ValueError('No currencies found.')
                LOG.info('Currencies are not empty')

                verified_curencies = currency_trafaret(json)
                LOG.info('All currencies passed check')

                return verified_curencies

    async def get_rates_on_date(self, date):
        '''
        Get rates on specific date. If date not provided
        today's date will be used.

        :param date: date to request rates for, defaults to today's date
        :type date: Arrow, optional

        :return: list of dicts with currencies
        :rtype: list

        '''
        requested_date = date.format('YYYYMMDD')
        params = f'date={requested_date}&json'
        LOG.info(f'Will request rates for {requested_date}')

        try:
            response = await self._get(self.bank_url, params)
            if self.date != arrow.now():
                self.date = self.date.shift(days=1)
        except ResponseCodeError:
            sys.exit(1)
        except trafaret.DataError:
            sys.exit(1)
        except ValueError:
            sys.exit(1)

        return response

    async def dump_rates_to_db(self, engine, rates):
        '''
        Save provided rates to specified file

        :param filename: name of the file to save to
        :type filename: str

        :param rates: rates to save
        :type rates: list

        '''

        LOG.info('Converted currencies to a dict')
        try:
            await self.psql.fill_tables(engine, rates)
        except:
            LOG.error()
            msg = 'Could not save date to db.'
            print(msg)
            LOG.error(msg)
        LOG.info('Dumped data to db')

    async def process(self, recursive):
        dates_to_process = arrow.Arrow.interval('days', self.date, arrow.now())
        async with self.psql.engine as engine:
            if recursive:
                for date in dates_to_process:
                    date = date[0]
                    LOG.info(f'Processing {date.format("DD.MM.YYYY")}')
                    date_rate = await self.get_rates_on_date(date)
                    await self.dump_rates_to_db(engine, date_rate)
                    LOG.info(f'Succesfully processed {date.format("DD.MM.YYYY")}')
            else:
                LOG.info(f'Processing {self.date.format("DD.MM.YYYY")}')
                date_rate = await self.get_rates_on_date(self.date)
                await self.dump_rates_to_db(engine, date_rate)
                LOG.info(f'Succesfully processed {self.date.format("DD.MM.YYYY")}')


def validate_date(ctx, param, value):
    '''
    Validate date that was set as a start date for getting rates

    :param value: date to verify
    :type value: str

    :raises ValueError: If format of the date is wrong

    :return: date to start with
    :rtype: Arrow

    '''
    
    try:
        start_date = arrow.get(value, 'DD-MM-YYYY')
    except ValueError:
        msg = 'Wrong format for the date. Expected DD-MM-YYYY (20-07-2021)\n'\
                'Use rates-cli -h for help.'
        print(msg)
        LOG.error(msg)
        sys.exit(1)

    if start_date > arrow.now():
        msg = 'This date has not came yet.'
        print(msg)
        LOG.error(msg)
        sys.exit(1)

    return start_date


async def start(ctx):
    '''
    Main function that runs client

    :param ctx: dictionary to hold extra user defined state
    :type ctx: Dict

    '''
    log_date = arrow.now().format('DD_MM')

    try:
        os.mkdir('log')
    except FileExistsError:
        pass

    logging.basicConfig(
        filename=f'rates_{log_date}.log',
        filemode='w',
        format='[%(asctime)s] [%(module)s] [%(levelname)-4s] [%(funcName)s] %(message)s',
        level='INFO',
    )

    bc = BankClient(ctx.get('date'))
    await bc.process(ctx.get('recursive'))


class WorkerSettings:
    cron_jobs = [arq.cron(start, minute=list(range(0, 60, 5)), run_at_startup=True)]


@click.command()
@click.option('--date', '-d', help='Date to parse.', callback=validate_date)
@click.option('--recursive', '-r', is_flag=True, help='Flag to parse all of the dates in range from --date up to today')
def main(date, recursive):
    ctx = {'date': date, 'recursive': recursive}
    if recursive:
        arq.run_worker(WorkerSettings, ctx=ctx)
    else:
        asyncio.run(start(ctx=ctx))
