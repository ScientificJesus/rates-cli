import json
import logging
import sys
import os

import sqlalchemy as sa
import trafaret
from aiopg.sa import create_engine
from psycopg2.errors import DuplicateTable
from sqlalchemy.sql.ddl import CreateTable

from ..models.models import currency_model
from .trafarets import config_trafaret

LOG = logging.getLogger(__name__)


class Postgresql:
    def __init__(self):
        config = Postgresql.process_conf()

        self.engine = create_engine(
            user=config["user"],
            database=config["database"],
            host=config["host"],
            password=config["password"],
        )

    @staticmethod
    def process_conf():
        """
        Function to read existing configuration or create new one

        :return: dict with credentials to use for DB
        :rtype: dict

        """

        if not os.path.isfile("config.json"):
            with open("config.json", "w+") as config:
                cfg_template = {
                    "user": "exchanger",
                    "database": "rates",
                    "host": "127.0.0.1",
                    "password": "exchanger",
                }

                print("Provide credentials for PostgresDB. They will be saved in config.json file")

                for key in cfg_template.keys():
                    cfg_template[key] = input(f"{key}: ")

                json.dump(cfg_template, config, indent=4)

        with open("config.json", "r") as cfg:
            config = json.loads(cfg.read())

        try:
            config_trafaret(config)
        except trafaret.DataError:
            msg = "Config did not pass check."
            print(msg)
            LOG.error(msg)
            sys.exit(1)

        return config

    async def recreate_tables(self, engine):
        """
        Creates table 'currencies'

        :param engine: Engine used for db
        :type engine: Engine
        """

        async with engine.acquire() as conn:
            await conn.execute(f"DROP TABLE IF EXISTS {currency_model.name}")
            LOG.info("Dropped existing table")
            await self._create_table(conn, currency_model)

    async def _create_table(self, conn, curr_model):
        """
        Creates table using sqlalchemy Table

        :param conn: current connection
        :type conn: SAConnection

        :param curr_model: model to create table from
        :type curr_model: Table

        """

        try:
            await conn.execute(CreateTable(curr_model))
            LOG.info("Created new table")
        except DuplicateTable:
            pass

    async def _insert_tbl(self, conn, table, currency_dict):
        """
        Method for inserting data into table

        :param conn: current connection
        :type conn: SAConnection

        :param table: table to insert data to
        :type table: Table

        :param currency_dict: dict with data on One currency
        :type currency_dict: dict

        """

        await conn.execute(table.insert().values(currency_dict))
        LOG.info(f"Inserted new data: {currency_dict} to table")

    async def fill_tables(self, engine, data):
        """
        Fill tables with new data

        :param data: currency with values on date
        :type data: list

        """

        async with engine.acquire() as conn:
            # await self._create_table(conn, currency_model)
            for currency in data:
                await self._insert_tbl(conn, currency_model, currency)
