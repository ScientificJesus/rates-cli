import trafaret as t
import arrow

currency_trafaret = t.List(t.Dict({
    t.Key('r030') >> 'code': t.Int(),
    t.Key('txt') >> 'name': t.String(),
    t.Key('rate'): t.Float(),
    t.Key('cc') >> 'currency_code': t.String,
    t.Key('exchangedate') >> 'date': t.ToDate(format='%d.%m.%Y') >> arrow.get
}))

config_trafaret = t.Dict({
    'user': t.String(),
    'database': t.String(),
    'host': t.String(),
    'password': t.String()
})
