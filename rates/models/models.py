from sqlalchemy import Table, Column, Integer, String, Float
from sqlalchemy_utils import ArrowType

from ..utils.metadata import metadata


currency_model = Table(
    'currencies',
    metadata,
    Column('id', Integer(), primary_key=True),
    Column('code', Integer()),
    Column('name', String()),
    Column('rate', Float(5)),
    Column('currency_code', String(3)),
    Column('date', ArrowType())
)
